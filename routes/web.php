<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/* @var \Laravel\Lumen\Routing\Router $router */

$router->group(['prefix' => '/api/v1'], function () use ($router){
    $router->group(['prefix' => '/auth'], function () use ($router) {
        //Authenticate
        $router->post('/register', 'AuthController@registerAction');
        $router->get('/confirm/{api_token}', 'AuthController@confirmAction');
        $router->post('/login',  'AuthController@loginAction');
        $router->post('/change_password',  'AuthController@changePassword');
        $router->post('/password_recovery',  'AuthController@passwordRecovery');//Not working
    });

    $router->group(['middleware' => 'auth:api'],function () use ($router) {
        //directories
        $router->group(['prefix' => '/directories'], function () use ($router) {
            $router->get('/', 'DirectoriesController@listAction');//Как отображать только is_master у pivot
            $router->post('/',  'DirectoriesController@createAction');
            $router->group(['prefix' => '/{directoryUuid}'], function () use ($router) {
                $router->get('/', 'DirectoriesController@getAction');
                $router->put('/', 'DirectoriesController@changeNameAction');
                $router->delete('/', 'DirectoriesController@deleteAction');
                $router->post('/share', 'DirectoriesController@shareAction');
                $router->delete('/leave', 'DirectoriesController@leaveAction');
            });
        });

        //files
        $router->group(['prefix' => '/directories/{directoryUuid}/files'], function () use ($router) {
            $router->get('/', 'FilesController@listAction');//checked work
            $router->post('/',  'FilesController@uploadAction');
            $router->group(['prefix' => '/{fileUuid}'], function () use ($router) {
                $router->get('/', 'FilesController@downloadAction');//Not working
                $router->put('/', 'FilesController@editAction');
                $router->delete('/', 'FilesController@deleteAction');
                $router->post('/move/', 'FilesController@moveAction');
                $router->post('/copy/', 'FilesController@copyAction');//Not working
            });
        });
    });
});
