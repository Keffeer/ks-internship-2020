<?php

return[
    'file_min_lifetime_sec' => env('FILE_MIN_LT_SEC'),
    'file_max_lifetime_sec' => env('FILE_MAX_LT_SEC'),
    'file_max_size_kb' => env('FILE_MAX_SIZE_KB'),
    'confirmed_account_max_size' => env('CONF_ACC_MAX_SIZE'),
    'not_confirmed_account_max_size' => env('NOT_CONF_ACC_MAX_SIZE'),
    'files_directory' => env('FILES_DIRECTORY'),
    'shares_limit' => env('LIMIT_FOR_SHARING'),
];
