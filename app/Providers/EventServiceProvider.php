<?php

namespace App\Providers;

use App\Events\DirectorySharedEvent;
use App\Events\ExampleEvent;
use App\Events\InviteEvent;
use App\Events\UserAddedFileEvent;
use App\Events\UserRegisteredEvent;
use App\Listeners\ExampleListener;
use App\Listeners\StatisticListener;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        DirectorySharedEvent::class => [
            StatisticListener::class,
        ],
        UserRegisteredEvent::class => [
            StatisticListener::class,
        ],
        UserAddedFileEvent::class => [
            StatisticListener::class,
        ],
        InviteEvent::class => [
            StatisticListener::class,
        ],
    ];
}
