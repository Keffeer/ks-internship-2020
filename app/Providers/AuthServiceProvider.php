<?php

namespace App\Providers;

use App\Exceptions\NotFoundModelException;
use App\Exceptions\BaseException;
use App\Exceptions\UserNotFoundException;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function (Request $request) {
            try {
                if ($request->hasHeader('api_token')) {
                    return User::query()->where(['api_token' => $request->header('api_token')])->firstOrFail();
                }
            }catch (\Throwable $exception) {
                throw new UserNotFoundException($exception->getMessage(), $exception->getCode());
            }
        });
    }
}
