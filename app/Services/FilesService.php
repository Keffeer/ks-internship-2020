<?php


namespace App\Services;

use App\DTO\CopyFile;
use App\DTO\SaveFile;
use App\Events\UserAddedFileEvent;
use App\Exceptions\FileNotSavedException;
use App\Exceptions\ModelNotSavedException;
use App\Exceptions\ModelsNameExistsException;
use App\Exceptions\TotalFilesLimitException;
use App\Models\Directory;
use App\Models\File as FileModel;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Throwable;


/**
 * Class FilesService
 * @package App\Services
 */
class FilesService
{

    private int $minLifetimeSeconds;
    private int $maxLifetimeSeconds;
    private int $confirmedAccMaxSize;
    private int $nonConfirmedAccMaxSize;
    private string $FilesDirectory;
    private const DIRECTORY_SEPARATOR = '/';

    public function __construct()
    {
        $config= config('exchanger');
        $this->minLifetimeSeconds = $config['file_min_lifetime_sec'];
        $this->maxLifetimeSeconds =  $config['file_max_lifetime_sec'];
        $this->confirmedAccMaxSize =  $config['confirmed_account_max_size'];
        $this->nonConfirmedAccMaxSize =  $config['not_confirmed_account_max_size'];
        $this->FilesDirectory = $config['files_directory'];
    }

    /**
     * @param User $user
     * @param Directory $directory
     * @param FileModel $file
     * @throws Exception
     */
    public function deleteFiles(User $user, Directory $directory, FileModel $file): void
    {
        $filePath = $this->createPath($this->FilesDirectory, $user->username, $directory->name, $file->filename);
        unlink($filePath);
        $file->delete();
    }

    /**
     * @param SaveFile $File
     * @return FileModel
     * @throws ModelNotSavedException
     * @throws TotalFilesLimitException
     * @throws FileNotSavedException
     * @throws ModelsNameExistsException
     */
    public function saveFiles(SaveFile $File): FileModel
    {
        $this->checkUniqueNameFileInDirectory($File->getDirectory(),
                                              $File->getFile()->getClientOriginalName(),
                                              $File->getFilename());

        $fileSizeKb = intdiv($File->getFile()->getSize(), 1024);
        $this->checkUserMaxSize($File->getUser(), $fileSizeKb);
        $this->checkLifeTimeSeconds($File->getLifetimeSeconds());


        $fileModel = new FileModel([
        'uuid' => Str::uuid(),
        'name' => $File->getFilename(),
        'filename' => $File->getFile()->getClientOriginalName(),
        'lifetime_seconds' => $File->getLifetimeSeconds(),
        'file_size' => $fileSizeKb
        ]);
        DB::beginTransaction();
        try {
            $fileModel ->saveOrFail();
            $File->getDirectory()->files()->attach($fileModel->id);
            $filePath = $this->createPath($this->FilesDirectory,
                                          $File->getUser()->username,
                                          $File->getDirectory()->name);
            $File->getFile()->move($filePath, $fileModel->filename);
        }
        catch (FileException $exception){
            DB::rollBack();
            throw new FileNotSavedException($exception->getMessage());
        }
        catch (Throwable $exception){
            DB::rollBack();
            throw new ModelNotSavedException($exception->getMessage());
        }
        DB::commit();
        $File->getUser()->changeCurrentFilesSize($fileSizeKb, true);

        event(new UserAddedFileEvent($File->getUser(), $File->getDirectory(), $fileModel));

        return $fileModel;
    }

    /**
     * @param CopyFile $File
     * @return void
     * @throws FileNotSavedException
     * @throws ModelsNameExistsException
     */
    public function moveFiles(CopyFile $File): void
    {
        $this->checkUniqueNameFileInDirectory($File->getNewDirectory(),
                                              $File->getFile()->filename,
                                              $File->getFile()->name);
        $oldPath = $this->createPath($this->FilesDirectory, $File->getUser()->username,
                                     $File->getOldDirectory()->name, $File->getFile()->filename);
        $newPath = $this->createPath($this->FilesDirectory, $File->getUser()->username, $File->getNewDirectory()->name);

        $uploadedFile = new UploadedFile($oldPath, $File->getFile()->filename,null,null,true);
        try {
            $uploadedFile->move($newPath, $File->getFile()->filename);
            $File->getFile()->directories()->attach(['directory_id'=>$File->getNewDirectory()->id]);
            DB::table('files_directories')
                ->where(['directory_id'=>$File->getOldDirectory()->id, 'file_id'=> $File->getFile()->id])
                ->delete();
        }
        catch (Throwable $exception){
            throw new FileNotSavedException($exception->getMessage());
        }
    }

    /**
     * @param string ...$parts
     * @return string
     */
    private function createPath(string ...$parts):string
    {
        return implode(DIRECTORY_SEPARATOR, $parts);
    }

    /**
     * @param int $lifetime_seconds
     * @throws TotalFilesLimitException
     */
    private function checkLifeTimeSeconds(int $lifetime_seconds):void
    {
        if($lifetime_seconds > $this->maxLifetimeSeconds || $lifetime_seconds < $this->minLifetimeSeconds) {
            throw new TotalFilesLimitException('File life time over the limit!');
        }
    }

    /**
     * @param User $user
     * @param int $fileSizeKb
     * @throws TotalFilesLimitException
     */
    private function checkUserMaxSize(User $user, int $fileSizeKb):void
    {
        $currentSize = $user->getCurrentFilesSize();
        $newTotalSize = $currentSize + $fileSizeKb;
        if($user->confirmed &&  $newTotalSize < $this->confirmedAccMaxSize) {
            return;
        }
        if(!$user->confirmed && $newTotalSize < $this->nonConfirmedAccMaxSize) {
            return;
        }
        throw new TotalFilesLimitException('File total size limit!');
    }

    /**
     * @param Directory $directory
     * @param string $fileName
     * @param string $virtualName
     * @throws ModelsNameExistsException
     */
    private function checkUniqueNameFileInDirectory(Directory $directory, string $fileName, string $virtualName): void
    {
        $checkFilename = $directory
            ->files()
            ->where(['filename' =>$fileName])
            ->exists();
        $checkName = $directory
            ->files()
            ->where(['name' => $virtualName])
            ->exists();
        if ($checkFilename || $checkName) {
            throw new ModelsNameExistsException('File name or virtual name already exists in that directory');
        }
    }

    /**
     * @param User $user
     * @param Directory $directory
     * @param FileModel $file
     * @return UploadedFile
     */
    public function DownloadFile(User $user, Directory $directory, FileModel $file): UploadedFile
    {
        $filePath = $this->createPath($this->FilesDirectory, $user->username, $directory->name, $file->filename);
        return new UploadedFile($filePath,  $file->filename, null, null, true);
    }
}
