<?php


namespace App\Services;

use App\Events\DirectorySharedEvent;
use App\Events\InviteEvent;
use App\Exceptions\BaseException;
use App\Exceptions\EmailHasInviteOnDirectoryException;
use App\Exceptions\NotFoundModelException;
use App\Exceptions\ShareNotSaveException;
use App\Exceptions\TotalUsersShareLimitException;
use App\Exceptions\UserNotConfirmedException;
use App\Exceptions\UserShareOnSelfException;
use App\Exceptions\UserSlaveOnDirectoryException;
use App\Mail\SendInvite;
use App\Models\Directory;
use App\Models\Invite;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\True_;

/**
 * Class ShareService
 * @package App\Services
 */
class ShareService
{

    private int $LimitShare;

    /**
     * ShareService constructor.
     */
    public function __construct()
    {
        $this->LimitShare = config('exchanger.shares_limit');
    }

    /**
     * @param User $slave
     * @param string $directoryUuid
     * @throws BaseException
     * @throws NotFoundModelException
     */
    public function leaveDirectory(User $slave, string $directoryUuid): void
    {
        $directory = $this->checkUserIsMasterOrSlave($slave, $directoryUuid, false);
        DB::table('users_directories')
            ->where(['user_id' => $slave->id, 'directory_id'=> $directory->id,'is_master' => false])
            ->delete();

        if($this->checkShareDirectory($directory))
            return;

        /* @var User $owner */
        try {
            $owner = $directory
                ->users()
                ->wherePivot('is_master', true)
                ->firstOrFail();
            $owner->changeCurrentCountShare(false);
        }catch (\Throwable $exception){
            throw new BaseException($exception->getMessage());
        }
    }

    /**
     * @param User $owner
     * @param string $directoryUuid
     * @param string $shareUserName
     * @throws NotFoundModelException
     * @throws ShareNotSaveException
     * @throws TotalUsersShareLimitException
     * @throws UserShareOnSelfException
     * @throws UserSlaveOnDirectoryException
     * @throws EmailHasInviteOnDirectoryException|UserNotConfirmedException
     */
    public function shareDirectory(User $owner, string $directoryUuid, string $shareUserName): void
    {
        if(!$owner->confirmed){
            throw new UserNotConfirmedException('User can not share directory if he is not confirmed');
        }
        $directory = $this->checkUserIsMasterOrSlave($owner, $directoryUuid, true);

        $alreadyShared = $this->checkShareDirectory($directory);
        if(!$alreadyShared)
            $this->checkUsersMaxShare($owner);

        $this->checkUserNotShareOnSelf($owner, $shareUserName);

        $newUser = User::query()->where('username', $shareUserName)->first();
        if($newUser !== null) {
            $this->shareDirectoryOnUser($newUser, $directory, $shareUserName);
        }
        else {
            $this->inviteNewUser($owner, $directory, $shareUserName);
            $newUser = new User(['username'=>$shareUserName]);
        }

        if(!$alreadyShared)
            $owner->changeCurrentCountShare(true);

        event(new DirectorySharedEvent($owner, $directory, $newUser));

    }

    /**
     * @param User $user
     * @param string $directoryUuid
     * @param bool $isMaster
     * @return Directory
     * @throws NotFoundModelException
     */
    private function checkUserIsMasterOrSlave(User $user, string $directoryUuid, bool $isMaster): Directory
    {
        try {
            $directory = $user
                ->directories()
                ->where('uuid', $directoryUuid)
                ->wherePivot('is_master', $isMaster)
                ->firstOrFail();
        }catch(\Throwable $exception){
            throw new NotFoundModelException('User is not a master\slave or do not have that directory or directory is not exists');
        }
        return $directory;
    }

    /**
     * @param Directory $directory
     * @return bool
     */
    private function checkShareDirectory(Directory $directory): bool
    {
        $checkDirectoryShare =  $directory
            ->users()
            ->wherePivot('is_master', false)
            ->exists();
        $checkInviteOnDirectory =  $directory->invites()->exists();
        return $checkDirectoryShare || $checkInviteOnDirectory;
    }

    /**
     * @param User $user
     * @throws TotalUsersShareLimitException
     */
    private function checkUsersMaxShare(User $user): void
    {
        if($user->getCurrentCountShare() >= $this->LimitShare)
            throw new TotalUsersShareLimitException('User have limited count of share');

    }

    /**
     * @param User $user
     * @param string $shareUserName
     * @throws UserShareOnSelfException
     */
    private function checkUserNotShareOnSelf(User $user, string $shareUserName): void
    {
        if($user->username === $shareUserName)
            throw new UserShareOnSelfException('User share directory on self');

    }

    /**
     * @param User $owner
     * @param Directory $directory
     * @param string $shareUserName
     * @throws EmailHasInviteOnDirectoryException
     * @throws ShareNotSaveException
     */
    private function inviteNewUser(User $owner, Directory $directory,  string $shareUserName): void
    {
        if($directory->invites()->where(['invitee_email'=> $shareUserName])->exists())
            throw new EmailHasInviteOnDirectoryException('This email already have invite to that directory');

        $invite = new Invite([
            'invitee_email' => $shareUserName,
            'token'=> Str::random()
        ]);
        DB::beginTransaction();
        try {
            $invite->users()->associate($owner);
            $invite->saveOrFail();
            $invite->directories()->attach(['directory_id' => $directory->id]);
            Mail::to($shareUserName)->send(new SendInvite($invite));
        }
        catch (\Throwable $exception){
            DB::rollBack();
            throw new ShareNotSaveException($exception->getMessage(), $exception->getCode());
        }
        DB::commit();
        event(new InviteEvent($owner, $invite));

    }

    /**
     * @param User $newUser
     * @param Directory $directory
     * @param string $shareUserName
     * @throws UserSlaveOnDirectoryException
     */
    private function shareDirectoryOnUser(User $newUser, Directory $directory, string $shareUserName): void
    {
        if($directory->users()->where(['username'=> $shareUserName])->exists())
            throw new UserSlaveOnDirectoryException('User already have access to that directory');

        $directory->users()->attach(['user_id' => $newUser->id], ['is_master' => false]);
    }

    /**
     * @param User $user
     * @throws NotFoundModelException
     * @throws Exception
     */
    public function shareDirectoryOnInvite(User $user): void
    {
        /* @var Invite $invite */
        try {
            $invites = Invite::query()->where(['invitee_email' => $user->username])->get();
        }catch (\Throwable $exception){
            throw new NotFoundModelException($exception->getMessage());
        }
        foreach ($invites as $invite){
            $user->directories()->attach(['directory_id'=>$invite->directories()->first()->id], ['is_master'=>false]);
            $invite->delete();
        }
    }

}
