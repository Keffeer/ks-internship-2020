<?php


namespace App\DTO;


use App\Models\Directory;
use App\Models\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SaveFile
{
    private User $user;
    private Directory $directory;
    private UploadedFile $file;
    private string $filename;
    private int $lifetime_seconds;

    /**
     * SaveFile constructor.
     * @param User $user
     * @param Directory $directory
     * @param UploadedFile $file
     * @param string $filename
     * @param int $lifetime_seconds
     */
    public function __construct(User $user, Directory $directory, UploadedFile $file, string $filename, int $lifetime_seconds)
    {
        $this->user = $user;
        $this->directory = $directory;
        $this->file = $file;
        $this->filename = $filename;
        $this->lifetime_seconds = $lifetime_seconds;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Directory
     */
    public function getDirectory(): Directory
    {
        return $this->directory;
    }

    /**
     * @param Directory $directory
     */
    public function setDirectory(Directory $directory): void
    {
        $this->directory = $directory;
    }

    /**
     * @return UploadedFile
     */
    public function getFile(): UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file): void
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @return int
     */
    public function getLifetimeSeconds(): int
    {
        return $this->lifetime_seconds;
    }

}
