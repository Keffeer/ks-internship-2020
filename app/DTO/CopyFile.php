<?php


namespace App\DTO;


use App\Models\Directory;
use App\Models\File;
use App\Models\User;

class CopyFile
{
    private User $user;
    private Directory $oldDirectory;
    private Directory $newDirectory;
    private File $file;

    /**
     * CopyFile constructor.
     * @param User $user
     * @param Directory $oldDirectory
     * @param Directory $newDirectory
     * @param File $file
     */
    public function __construct(User $user, Directory $oldDirectory, Directory $newDirectory, File $file)
    {
        $this->user = $user;
        $this->oldDirectory = $oldDirectory;
        $this->newDirectory = $newDirectory;
        $this->file = $file;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Directory
     */
    public function getOldDirectory(): Directory
    {
        return $this->oldDirectory;
    }

    /**
     * @param Directory $oldDirectory
     */
    public function setOldDirectory(Directory $oldDirectory): void
    {
        $this->oldDirectory = $oldDirectory;
    }

    /**
     * @return Directory
     */
    public function getNewDirectory(): Directory
    {
        return $this->newDirectory;
    }

    /**
     * @param Directory $newDirectory
     */
    public function setNewDirectory(Directory $newDirectory): void
    {
        $this->newDirectory = $newDirectory;
    }

    /**
     * @return File
     */
    public function getFile(): File
    {
        return $this->file;
    }

    /**
     * @param File $file
     */
    public function setFile(File $file): void
    {
        $this->file = $file;
    }

}
