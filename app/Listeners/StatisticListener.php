<?php


namespace App\Listeners;


use App\Events\ExampleEvent;
use App\Events\StatisticEvent;
use App\Services\QueueService;

class StatisticListener
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param StatisticEvent $event
     * @return void
     */
    public function handle(StatisticEvent $event): void
    {
        QueueService::sendMessage($event->getArrayOfData());
    }
}
