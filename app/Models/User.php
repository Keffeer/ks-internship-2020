<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Cache;
use Laravel\Lumen\Auth\Authorizable;

/**
 * Class User
 * @package App\Models
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    public const TOTAL_FILES_CACHE_PREFIX = 'Exchanger::totalFiles::';
    public const TOTAL_USER_SHARE = 'Exchanger::totalShare::';
    public const TOTAL_DIRECTORY_CACHE_PREFIX = 'Exchanger::totalDirectories::';


    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'api_token'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id','username', 'password_hash', 'confirmed', 'created_at', 'updated_at'
    ];

    /**
     * @return BelongsToMany
     */
    public  function directories(): BelongsToMany
    {
        return $this
            ->belongsToMany(Directory::class, 'users_directories', 'user_id', 'directory_id')
            ->withPivot('is_master');
    }

    public function invites(): HasMany
    {
        return $this->hasMany(Invite::class);
    }

    /**
     * @return int
     */
    public function getCurrentFilesSize(): int
    {
        return (int)Cache::get(self::TOTAL_FILES_CACHE_PREFIX . $this->id);
    }

    /**
     * @param int $fileSizeKb
     * @param bool $UpOrDown
     */
    public function changeCurrentFilesSize(int $fileSizeKb, bool $UpOrDown): void
    {
        if($UpOrDown) {
            Cache::store('redis')->increment(self::TOTAL_FILES_CACHE_PREFIX . $this->id, $fileSizeKb);
            return;
        }
        Cache::store('redis')->increment(self::TOTAL_FILES_CACHE_PREFIX . $this->id, -$fileSizeKb);
    }


    /**
     * @return int
     */
    public function getCurrentCountShare(): int
    {
        return (int)Cache::get(self::TOTAL_USER_SHARE . $this->id);
    }

    /**
     * @param bool $addOrDelete
     */
    public function changeCurrentCountShare(bool $addOrDelete): void
    {
        if($addOrDelete) {
            Cache::store('redis')->increment(self::TOTAL_USER_SHARE . $this->id, 1);
            return;
        }
        Cache::store('redis')->increment(self::TOTAL_USER_SHARE . $this->id, -1);

    }

    /**
     * @return int
     */
    public function getCountOfDirectory(): int
    {
        return (int)Cache::get(self::TOTAL_DIRECTORY_CACHE_PREFIX . $this->id);
    }

    /**
     * @param bool $UpOrDown
     */
    public function changeCountOfDirectory(bool $UpOrDown): void
    {
        if($UpOrDown) {
            Cache::store('redis')->increment(self::TOTAL_DIRECTORY_CACHE_PREFIX . $this->id, 1);
            return;
        }
        Cache::store('redis')->increment(self::TOTAL_DIRECTORY_CACHE_PREFIX . $this->id, -1);
    }


}
