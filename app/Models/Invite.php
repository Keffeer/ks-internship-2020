<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Invite
 * @package App\Models
 */
class Invite extends Model{
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'invites';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'token', 'invitee_email'
    ];

    /**
     * @return BelongsToMany
     */
    public  function directories(): BelongsToMany
    {
        return $this
            ->belongsToMany(Directory::class, 'invites_shared_directories', 'invite_id', 'directory_id');
    }

    public  function users(): BelongsTo
    {
        return $this
            ->belongsTo(User::class, 'inviter_user_id');
    }

}
