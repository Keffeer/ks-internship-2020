<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Directory
 * @package App\Models
 */
class Directory extends Model{

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'directories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'uuid'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'created_at', 'updated_at'
    ];

    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this
            ->belongsToMany(User::class, 'users_directories',  'directory_id','user_id')
            ->withPivot('is_master');
    }

    /**
     * @return BelongsToMany
     */
    public function files(): BelongsToMany
    {
        return $this->belongsToMany(File::class, 'files_directories',  'directory_id','file_id');
    }

    /**
     * @return BelongsToMany
     */
    public function invites(): BelongsToMany
    {
        return $this->belongsToMany(Invite::class, 'invites_shared_directories',  'directory_id','invite_id');
    }
}
