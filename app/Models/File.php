<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class File
 * @package App\Models
 */
class File extends Model
{
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'uuid', 'filename', 'lifetime_seconds','file_size'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'pivot', 'filename', 'created_at', 'updated_at'
    ];

    /**
     * @return BelongsToMany
     */
    public function directories(): BelongsToMany
    {
        return $this->belongsToMany(Directory::class, 'files_directories',  'file_id','directory_id');
    }
}
