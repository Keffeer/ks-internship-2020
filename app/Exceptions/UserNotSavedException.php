<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class UserNotSavedException extends BaseException
{
    protected int $errorCode = ErrorCode::USER_NOT_SAVED;
    protected int $HttpStatusCode = Response::HTTP_BAD_REQUEST;
}
