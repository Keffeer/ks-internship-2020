<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class UserNotConfirmedException extends BaseException
{
    protected int $errorCode = ErrorCode::USER_NOT_CONFIRMED;
    protected int $HttpStatusCode = Response::HTTP_UNAUTHORIZED;
}
