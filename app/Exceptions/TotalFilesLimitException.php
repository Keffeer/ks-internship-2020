<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class TotalFilesLimitException extends BaseException
{
    protected int $HttpStatusCode = Response::HTTP_BAD_REQUEST;
    protected int $errorCode = ErrorCode::ACCOUNT_FILESIZE_LIMIT;
}
