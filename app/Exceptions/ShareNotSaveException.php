<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class ShareNotSaveException extends BaseException
{
    protected int $errorCode = ErrorCode::SHARE_NOT_SAVE;
    protected  int $HttpStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
}
