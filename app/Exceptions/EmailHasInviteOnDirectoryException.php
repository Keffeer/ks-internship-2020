<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class EmailHasInviteOnDirectoryException extends BaseException
{
    protected  int $errorCode = ErrorCode::EMAIL_HAS_INVITE_ON_THAT_DIRECTORY;
    protected  int $HttpStatusCode = Response::HTTP_FORBIDDEN;
}
