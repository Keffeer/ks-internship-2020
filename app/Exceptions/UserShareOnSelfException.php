<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class UserShareOnSelfException extends BaseException
{
    protected int $HttpStatusCode = Response::HTTP_FORBIDDEN;
    protected int $errorCode = ErrorCode::USER_SHARE_ON_SELF;
}
