<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Class BaseException
 * @package App\Exceptions
 */
class BaseException extends \Exception
{
    protected int $errorCode = ErrorCode::GENERAL_CODE;
    protected int $HttpStatusCode = Response::HTTP_BAD_REQUEST;

    /**
     * @return int
     */
    public function getErrorCode(): int
    {
        return $this->errorCode;
    }

    /**
     * @return int
     */
    public function getHttpException(): int
    {
        return $this->HttpStatusCode;
    }

    /**
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        return new JsonResponse([
            'success' => false,
            'message' => $this->getMessage(),
            'errorCode' => $this->getErrorCode(),
        ], $this->getHttpException());
    }
}
