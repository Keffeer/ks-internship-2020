<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class UserHaveOneDirectoryException extends BaseException
{
    protected int $errorCode = ErrorCode::USER_HAVE_ONE_DIRECTORY;
    protected int $HttpStatusCode = Response::HTTP_FORBIDDEN;
}
