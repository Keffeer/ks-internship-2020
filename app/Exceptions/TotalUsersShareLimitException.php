<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class TotalUsersShareLimitException extends BaseException
{
    protected int $HttpStatusCode = Response::HTTP_BAD_REQUEST;
    protected int $errorCode = ErrorCode::USER_SHARE_LIMIT;
}
