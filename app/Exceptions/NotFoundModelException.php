<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class NotFoundModelException extends BaseException
{
    protected  int $errorCode = ErrorCode::MODEL_NOT_FOUND;
    protected  int $HttpStatusCode = Response::HTTP_FORBIDDEN;
}
