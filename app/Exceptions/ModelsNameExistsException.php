<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class ModelsNameExistsException extends BaseException
{
    protected  int $errorCode = ErrorCode::MODEL_AlREADY_EXISTS;
    protected  int $HttpStatusCode = Response::HTTP_BAD_REQUEST;
}
