<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class ModelNotSavedException extends BaseException
{
    protected  int $errorCode = ErrorCode::MODEL_NOT_SAVED;
    protected  int $HttpStatusCode = Response::HTTP_FORBIDDEN;
}
