<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class LifeTimeLimitException extends BaseException
{
    protected int $HttpStatusCode = Response::HTTP_BAD_REQUEST;
    protected int $errorCode = ErrorCode::LIFE_TIME_LIMIT;
}
