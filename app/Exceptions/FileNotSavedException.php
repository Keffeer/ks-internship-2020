<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class FileNotSavedException extends BaseException
{
    protected  int $errorCode = ErrorCode::FILE_NOT_SAVED;
    protected  int $HttpStatusCode = Response::HTTP_CONFLICT;
}
