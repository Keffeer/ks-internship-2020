<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class UserExistsException extends BaseException
{
    protected int $errorCode = ErrorCode::USER_EXISTS;
    protected int $HttpStatusCode = Response::HTTP_FORBIDDEN;
}
