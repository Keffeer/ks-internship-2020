<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class UserNotAuthorizedException extends BaseException
{
    protected int $errorCode = ErrorCode::INCORRECT_PASSWORD;
    protected int $HttpStatusCode = Response::HTTP_UNAUTHORIZED;
}
