<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class UserSlaveOnDirectoryException extends BaseException
{
    protected  int $errorCode = ErrorCode::USER_ALREADY_SLAVE;
    protected  int $HttpStatusCode = Response::HTTP_FORBIDDEN;
}
