<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Illuminate\Http\Response;

class UserNotFoundException extends BaseException
{
    protected int $errorCode = ErrorCode::USER_NOT_FOUND;
    protected int $HttpStatusCode = Response::HTTP_FORBIDDEN;
}
