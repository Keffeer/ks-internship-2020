<?php


namespace App\Events;


use App\Models\Invite;
use App\Models\User;

class InviteEvent extends Event implements StatisticEvent
{

    private User $user;
    private Invite $invite;

    /**
     * InviteEvent constructor.
     * @param User $user
     * @param Invite $invite
     */
    public function __construct(User $user, Invite $invite)
    {
        $this->user = $user;
        $this->invite = $invite;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Invite
     */
    public function getInvite(): Invite
    {
        return $this->invite;
    }

    public function getArrayOfData(): array
    {
        return [
            'user' => $this->user->id,
            'invite' => $this->invite->id
        ];
    }
}
