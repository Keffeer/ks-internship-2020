<?php


namespace App\Events;

use App\Models\Directory;
use App\Models\User;

/**
 * Class DirectorySharedEvent
 * @package App\Events
 */
class DirectorySharedEvent extends Event implements StatisticEvent
{

    private User $owner;
    private Directory $directory;
    private User $invitee;

    /**
     * DirectorySharedEvent constructor.
     * @param User $owner
     * @param Directory $directory
     * @param User $invitee
     */
    public function __construct(User $owner, Directory $directory, User $invitee)
    {
        $this->owner = $owner;
        $this->directory = $directory;
        $this->invitee = $invitee;
    }

    /**
     * @return User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * @return Directory
     */
    public function getDirectory(): Directory
    {
        return $this->directory;
    }

    /**
     * @return User
     */
    public function getInvitee(): User
    {
        return $this->invitee;
    }

    public function getArrayOfData(): array
    {
        return [
            'owner' => $this->owner->id,
            'directory' => $this->directory->id,
            'invitee' => $this->invitee->id
        ];
    }
}
