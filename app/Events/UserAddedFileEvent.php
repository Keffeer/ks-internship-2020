<?php


namespace App\Events;


use App\Models\Directory;
use App\Models\File;
use App\Models\User;

class UserAddedFileEvent extends Event implements StatisticEvent
{

    private User $user;
    private Directory $directory;
    private File $file;

    /**
     * UserAddedFileEvent constructor.
     * @param User $user
     * @param Directory $directory
     * @param File $file
     */
    public function __construct(User $user, Directory $directory, File $file)
    {
        $this->user = $user;
        $this->directory = $directory;
        $this->file = $file;
    }


    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Directory
     */
    public function getDirectory(): Directory
    {
        return $this->directory;
    }

    /**
     * @return File
     */
    public function getFile(): File
    {
        return $this->file;
    }

    public function getArrayOfData(): array
    {
        return [
            'user' => $this->user->id,
            'directory' => $this->directory->id,
            'file' => $this->file->id
        ];
    }
}
