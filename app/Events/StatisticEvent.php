<?php


namespace App\Events;


interface StatisticEvent
{
    public function getArrayOfData(): array;
}
