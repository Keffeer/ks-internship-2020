<?php


namespace App\Events;


use App\Models\User;

class UserRegisteredEvent extends Event implements StatisticEvent
{

    private User $user;

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * UserRegisteredEvent constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getArrayOfData(): array
    {
        return ['user'=>$this->user->id];
    }
}
