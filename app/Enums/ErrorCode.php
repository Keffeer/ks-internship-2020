<?php

namespace App\Enums;

/**
 * Interface ErrorCode
 * @package App\Enums
 */
interface ErrorCode
{
    public const GENERAL_CODE = 1000;

    //AUTH
    public const USER_NOT_CONFIRMED= 1001;
    public const USER_NOT_FOUND = 1002;
    public const USER_NOT_SAVED = 1004;
    public const INCORRECT_PASSWORD = 1010;
    public const USER_EXISTS = 1003;

    //DIRECTORY
    public const USER_HAVE_ONE_DIRECTORY = 5001;

    //DIRECTORY AND FILE
    public const MODEL_AlREADY_EXISTS = 2003;
    public const MODEL_NOT_FOUND = 2002;
    public const MODEL_NOT_SAVED = 2004;

    //FILE
    public const FILE_NOT_SAVED = 3004;
    public const ACCOUNT_FILESIZE_LIMIT = 3500;
    public const LIFE_TIME_LIMIT = 3505;

    //SHARE
    public const USER_SHARE_LIMIT = 4001;
    public const USER_SHARE_ON_SELF = 4002;
    public const SHARE_NOT_SAVE = 4003;
    public const USER_ALREADY_SLAVE = 4004;
    public const EMAIL_HAS_INVITE_ON_THAT_DIRECTORY = 4005;

}
