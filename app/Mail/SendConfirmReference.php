<?php


namespace App\Mail;


use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendConfirmReference extends Mailable
{
    use Queueable, SerializesModels;

    private User $user;

    /**
     * SendInvite constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {
        return $this->view('emails.confirm')
            ->with([
                'api_token'=>$this->user->api_token,
            ]);
    }

}
