<?php


namespace App\Mail;


use App\Models\Invite;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendInvite extends Mailable
{
    use Queueable, SerializesModels;


    public Invite $invite;

    /**
     * SendInvite constructor.
     * @param Invite $invite
     */
    public function __construct(Invite $invite)
    {
        $this->invite = $invite;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {
        return $this->view('emails.invite')
                    ->with([
                        'username'=>$this->invite->invitee_email,
                        'invite_token' => $this->invite->token,
                        'inviter' => $this->invite->users()->where(['id'=>$this->invite->inviter_user_id])->first()->username,
                    ]);
    }

}
