<?php

namespace App\Http\Middleware;

use App\Exceptions\UserNotConfirmedException;
use Carbon\Carbon;
use Closure;
use Faker\Provider\DateTime;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use MongoDB\BSON\Timestamp;
use phpDocumentor\Reflection\Types\Boolean;


class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected Auth $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param string|null $guard
     * @return mixed
     * @throws UserNotConfirmedException
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guard($guard)->guest()){
            return response('Unauthorized.', 401);
        }
        $this->checkConfirmed();

        return $next($request);
    }

    /**
     * @throws UserNotConfirmedException
     */
    public function checkConfirmed(): void
    {
        $user = $this->auth->user();
        if($user->confirmed){
            return;
        }
        if(Carbon::now()->getTimestamp() - $user->created_at->getTimestamp() < env('MAX_TIME_TO_CONFIRM')){
            return;
        }
        throw new UserNotConfirmedException('User not confirmed! Time have finished yet');
    }
}
