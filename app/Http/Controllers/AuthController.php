<?php


namespace App\Http\Controllers;
use App\Events\UserAddedFileEvent;
use App\Events\UserRegisteredEvent;
use App\Exceptions\NotFoundModelException;
use App\Exceptions\UserExistsException;
use App\Exceptions\UserNotAuthorizedException;
use App\Exceptions\UserNotConfirmedException;
use App\Exceptions\UserNotFoundException;
use App\Exceptions\UserNotSavedException;
use App\Mail\SendConfirmReference;
use App\Models\User;
use App\Models\Directory;
use App\Services\ShareService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use \Throwable;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws UserExistsException
     * @throws UserNotSavedException
     * @throws ValidationException
     * @throws Exception
     */
    public function registerAction(Request $request): JsonResponse
    {
        //Validation
        $this->validate($request, [
            'username' => 'required|email|max:255',
            'password' => 'required|max:255',
            'invite_token' => 'max:255',
        ]);

        //Checking for the existence of a user with that username
        if(User::query()->where('username', $request->input('username'))->exists())
            throw new UserExistsException('User Already Exists!');

        //Create a new user
        $user = new User();
        $user->username    = $request->input('username');
        $user->password_hash = Hash::make($request->input('password'));
        $user->api_token = Str::random(32);

        $directory = new Directory([
        'uuid' => Str::uuid()->toString(),
        'name' => 'defaultDirectory'
        ]);

        DB::beginTransaction();
        try {
            $user->saveOrFail();
            $directory->saveOrFail();
            $user->directories()->attach(['directories_id'=> $directory->id],['is_master'=>true]);
        }catch (Throwable $exception) {
            DB::rollBack();
            throw new UserNotSavedException($exception->getMessage(), $exception->getCode());
        }
        $shareService = new ShareService();
        $shareService->shareDirectoryOnInvite($user);
        DB::commit();

        $user->changeCountOfDirectory(true);

        Mail::to($user->username)->send(new SendConfirmReference($user));
        event(new UserRegisteredEvent($user));

        return $this->respondSuccess($user->toArray());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws UserNotAuthorizedException
     * @throws UserNotFoundException
     * @throws ValidationException
     */
    public function loginAction(Request $request): JsonResponse
    {
        //Validation
        $this->validate($request, [
            'username' => 'required|email|max:255',
            'password' => 'required',
        ]);

        //Check username
        /* @var User $user */
        try {
            $user = User::query()->where('username', $request->input('username'))->firstOrFail();
        }catch(Throwable $exception){
            throw new UserNotFoundException($exception->getMessage(), $exception->getCode());
        }

        //Check password
        if (!Hash::check($request->input('password'), $user->password_hash))
            throw new UserNotAuthorizedException('Incorrect password');

        return $this->respondSuccess($user->toArray());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws UserNotAuthorizedException
     * @throws UserNotFoundException
     * @throws UserNotSavedException
     * @throws ValidationException
     */
    public function changePassword(Request $request): JsonResponse
    {
        //Validation
        $this->validate($request, [
            'api_token' => 'required',
            'old_password' => 'required',
            'new_password' => 'required'
        ]);

        //Check api_token
        /* @var User $user */
        try {
            $user = User::query()->where('api_token', $request->input('api_token'))->firstOrFail();
        }catch(Throwable $exception){
            throw new UserNotFoundException($exception->getMessage(), $exception->getCode());
        }

        //Check password
        if (!Hash::check($request->input('old_password'), $user->password_hash))
            throw new UserNotAuthorizedException('Incorrect old password');

        $user->password_hash = Hash::make($request->input('new_password'));
        $user->api_token = Str::random(32);
        try {
            $user->saveOrFail();
        }catch (Throwable $exception) {
            throw new UserNotSavedException($exception->getMessage(), $exception->getCode());
        }
        return $this->respondSuccess($user->toArray());
    }


    /**
     * @param string $api_token
     * @return JsonResponse
     * @throws UserNotConfirmedException
     * @throws UserNotFoundException
     */
    public function confirmAction(string $api_token): JsonResponse
    {
        /* @var User $user */
        try {
            $user = User::query()->where('api_token',  $api_token)->firstOrFail();
        }catch(Throwable $exception){
            throw new UserNotFoundException($exception->getMessage(), $exception->getCode());
        }
        $user->confirmed = true;
        try {
            $user->saveOrFail();
        }catch (Throwable $exception) {
            throw new UserNotConfirmedException($exception->getMessage(), $exception->getCode());
        }
        return $this->respondSuccess($user->toArray());
    }
}
