<?php


namespace App\Http\Controllers;

use App\DTO\CopyFile;
use App\DTO\SaveFile;
use App\Exceptions\FileNotSavedException;
use App\Exceptions\LifeTimeLimitException;
use App\Exceptions\ModelsNameExistsException;
use App\Exceptions\NotFoundModelException;
use App\Exceptions\ModelNotSavedException;
use App\Exceptions\TotalFilesLimitException;
use App\Models\User;
use App\Models\Directory;
use App\Services\FilesService;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Throwable;
use App\Models\File as FileModel;

/**
 * Class FilesController
 * @package App\Http\Controllers
 */
class FilesController extends Controller
{
    /**
     * @var FilesService
     * @var User
     */
    private FilesService $service;
    private array $filesConfig;
    private User $user;


    /**
     * FilesController constructor.
     */
    public function __construct()
    {
        $this->filesConfig = config('exchanger');
        $this->service = new FilesService();
        $this->user = Auth::user();
    }

    /**
     * @param string $directoryUuid
     * @return JsonResponse
     * @throws NotFoundModelException
     */
    public function listAction(string $directoryUuid): JsonResponse
    {
        $directory = $this->getDirectory($directoryUuid);
        return $this->respondSuccess($directory->files->toArray());
    }

    /**
     * @param string $directoryUuid
     * @param Request $request
     * @return JsonResponse
     * @throws FileNotSavedException
     * @throws ModelNotSavedException
     * @throws NotFoundModelException
     * @throws TotalFilesLimitException
     * @throws ValidationException
     * @throws ModelsNameExistsException
     */
    public function uploadAction(string $directoryUuid, Request $request): JsonResponse
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'lifetime_seconds' => [
                'integer:' . $this->filesConfig['file_min_lifetime_sec']
                      .',' . $this->filesConfig['file_max_lifetime_sec']
            ],
            'file' => 'required|file:max'  . $this->filesConfig['file_max_size_kb']
        ]);

        $directory = $this->getDirectory($directoryUuid);
        /* @var UploadedFile $uploadedFile */
        $uploadedFile = $request->file('file');

        $saveFile = new SaveFile(
            $this->user,
            $directory,
            $uploadedFile,
            $request->post('name'),
            $request->post('lifetime_seconds'),
        );
        $file = $this->service->saveFiles($saveFile);

        return $this->respondSuccess($file->toArray());

    }

    /**
     * @param string $directoryUuid
     * @param string $fileUuid
     * @param Request $request
     * @return JsonResponse
     * @throws NotFoundModelException
     * @throws ValidationException
     * @throws LifeTimeLimitException
     * @throws ModelNotSavedException
     */
    public function editAction(string $directoryUuid, string $fileUuid, Request $request): JsonResponse
    {
        $this->validate($request, [
            'lifetime_seconds' => 'required|'.
                'integer:' . $this->filesConfig['file_min_lifetime_sec']
                      .',' . $this->filesConfig['file_max_lifetime_sec'],
        ]);
        $directory = $this->getDirectory($directoryUuid);
        try {
            $file = $directory->files()->where('uuid', $fileUuid)->firstOrFail();
        }catch (Throwable $exception){
            throw new NotFoundModelException($exception->getMessage());
        }
        $commonTimeToLive = Carbon::now()->getTimestamp()
                            - $file->created_at->getTimestamp()
                            + (int)$request->input('lifetime_seconds');
        $timeOverLimit = $commonTimeToLive > (int)$this->filesConfig['file_max_lifetime_sec'];
        if($timeOverLimit)
            throw new LifeTimeLimitException('The file cannot exist this amount of time');

        $file->lifetime_seconds = $commonTimeToLive;

        try{
            $file->saveOrFail();
        }catch (Throwable $exception){
            throw new ModelNotSavedException('File life time has not changed');
        }
        return $this->respondSuccess();
    }

    /**
     * @param string $directoryUuid
     * @param string $fileUuid
     * @param Request $request
     * @return JsonResponse
     * @throws NotFoundModelException
     * @throws Exception
     */
    public function deleteAction(string $directoryUuid, string $fileUuid, Request $request): JsonResponse
    {
        $directory = $this->getDirectory($directoryUuid);
        try {
            $file = $directory->files()->where('uuid', $fileUuid)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            throw new NotFoundModelException($exception->getMessage(), $exception->getCode());
        }
        $this->service->deleteFiles($this->user, $directory, $file);
        return $this->respondSuccess();
    }

    /**
     * @param string $directoryUuid
     * @param string $fileUuid
     * @param Request $request
     * @return JsonResponse
     * @throws FileNotSavedException
     * @throws ModelNotSavedException
     * @throws ModelsNameExistsException
     * @throws NotFoundModelException
     * @throws TotalFilesLimitException
     * @throws ValidationException
     * @throws Exception
     */
    public function copyAction(string $directoryUuid, string $fileUuid, Request $request): JsonResponse
    {
        //NOT WORKING
        $this->validate($request, [
            'uuid' => 'required|uuid',
        ]);

        $oldDirectory = $this->getDirectory($directoryUuid);
        $newDirectory = $this->getDirectory($request->post('uuid'));
        /* @var FileModel $file */
        try {
            $file = $oldDirectory->files()->where('uuid', $fileUuid)->firstOrFail();
        }catch (Throwable $exception){
            throw new NotFoundModelException('That file does not exist in that directory');
        }

        $uploadedFile = $this->service->DownloadFile($this->user, $oldDirectory, $file);

        $saveFile = new SaveFile(
            $this->user,
            $newDirectory,
            $uploadedFile,
            $file->name,
            $file->lifetime_seconds,
        );
        $newFile = $this->service->saveFiles($saveFile);
        return $this->respondSuccess($newFile->toArray());
    }

    /**
     * @param string $directoryUuid
     * @param string $fileUuid
     * @param Request $request
     * @return JsonResponse
     */
    public function downloadAction(string $directoryUuid, string $fileUuid, Request $request): JsonResponse
    {
        return $this->respondFailed('It is not work');
    }

    /**
     * @param string $directoryUuid
     * @param string $fileUuid
     * @param Request $request
     * @return JsonResponse
     * @throws FileNotSavedException
     * @throws NotFoundModelException
     * @throws ValidationException
     * @throws ModelsNameExistsException
     * @throws Exception
     */
    public function moveAction(string $directoryUuid, string $fileUuid, Request $request): JsonResponse
    {
        $this->validate($request, [
            'uuid' => 'required|uuid',
        ]);
        $oldDirectory = $this->getDirectory($directoryUuid);
        $newDirectory = $this->getDirectory($request->post('uuid'));

        /* @var FileModel $file */
        try {
            $file = $oldDirectory->files()->where('uuid', $fileUuid)->firstOrFail();
        }catch (Throwable $exception){
            throw new NotFoundModelException('That file does not exist in that directory');
        }

        $copyFile = new CopyFile(
            $this->user,
            $oldDirectory,
            $newDirectory,
            $file
        );
        $this->service->moveFiles($copyFile);

        return $this->respondSuccess();
    }

    /**
     * @param string $directoryUuid
     * @return Directory|Model|BelongsToMany|null
     * @throws NotFoundModelException
     */
    private function getDirectory(string $directoryUuid)
    {
        try {
            return $this->user->directories()->where('uuid', $directoryUuid)->firstOrFail();
        }catch (Throwable $exception){
            throw new NotFoundModelException($exception->getMessage(), $exception->getCode());
        }
    }

}
