<?php


namespace App\Http\Controllers;

use App\Exceptions\BaseException;
use App\Exceptions\EmailHasInviteOnDirectoryException;
use App\Exceptions\NotFoundModelException;
use App\Exceptions\ModelNotSavedException;
use App\Exceptions\ModelsNameExistsException;
use App\Exceptions\ShareNotSaveException;
use App\Exceptions\TotalUsersShareLimitException;
use App\Exceptions\UserHaveOneDirectoryException;
use App\Exceptions\UserNotConfirmedException;
use App\Exceptions\UserShareOnSelfException;
use App\Exceptions\UserSlaveOnDirectoryException;
use App\Models\Directory;
use App\Models\User;
use App\Services\ShareService;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use \Throwable;
use Illuminate\Validation\ValidationException;

/**
 * Class DirectoriesController
 * @package App\Http\Controllers
 */
class DirectoriesController extends Controller
{
    private User $user;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    /**
     * @return JsonResponse
     */
    public function listAction(): JsonResponse
    {
        return $this->respondSuccess($this->user->directories->toArray());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ModelNotSavedException
     * @throws ModelsNameExistsException
     * @throws ValidationException
     */
    public function createAction(Request $request): JsonResponse
    {
        //Validation
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        //Checking for the existence of a directory with that name
        if(Directory::query()->where(['name' => $request->input('name')])->exists())
            throw new ModelsNameExistsException('Directory Already Exists');

        $directory = new Directory([
        'uuid' => Str::uuid(),
        'name' =>  $request->input('name')]);

        DB::beginTransaction();

        try {
            $directory->saveOrFail();
            $directory->users()->attach(['user_id'=> $this->user->id],['is_master'=>true]);
        }catch (Throwable $exception) {
            DB::rollBack();
            throw new ModelNotSavedException($exception->getMessage(), $exception->getCode());
        }
        DB::commit();
        $this->user->changeCountOfDirectory(true);
        return $this->respondSuccess($directory->toArray());
    }

    /**
     * @param string $directoryUuid
     * @return JsonResponse
     * @throws NotFoundModelException
     */
    public function getAction(string $directoryUuid): JsonResponse
    {
        try {
            $directory = $this->user->directories()->where('uuid', $directoryUuid)->firstOrFail();
        }catch (Throwable $exception){
            throw new NotFoundModelException($exception->getMessage(), $exception->getCode());
        }
        return $this->respondSuccess($directory->toArray());
    }

    /**
     * @param Request $request
     * @param string $directoryUuid
     * @return JsonResponse
     * @throws ModelNotSavedException
     * @throws NotFoundModelException
     * @throws ValidationException
     */
    public function changeNameAction(Request $request, string $directoryUuid): JsonResponse
    {
        //Validation
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        try {
            $directory = $this->user->directories()->where('uuid', $directoryUuid)->firstOrFail();
            $directory->name = $request->input('name');
            $directory->saveOrFail();
        }
        catch (ModelNotFoundException $exception){
            throw new NotFoundModelException($exception->getMessage(), $exception->getCode());
        }
        catch (Throwable $exception){
            throw new ModelNotSavedException($exception->getMessage(), $exception->getCode());
        }
        return $this->respondSuccess();
    }

    /**
     * @param Request $request
     * @param string $directoryUuid
     * @return JsonResponse
     * @throws NotFoundModelException
     * @throws Exception
     * @throws UserHaveOneDirectoryException
     */
    public function deleteAction(Request $request, string $directoryUuid): JsonResponse
    {
        if($this->user->getCountOfDirectory()===1)
            throw new UserHaveOneDirectoryException('User can not delete it because it is last directory');

        try {
            $directory = $this->user->directories()->where('uuid', $directoryUuid)->firstOrFail();
            $directory->delete();
        }
        catch (ModelNotFoundException $exception) {
            throw new NotFoundModelException($exception->getMessage(), $exception->getCode());
        }
        $this->user->changeCountOfDirectory(false);
        return $this->respondSuccess();
    }

    /**
     * @param Request $request
     * @param string $directoryUuid
     * @return JsonResponse
     * @throws NotFoundModelException
     * @throws ShareNotSaveException
     * @throws TotalUsersShareLimitException
     * @throws UserShareOnSelfException
     * @throws ValidationException
     * @throws UserSlaveOnDirectoryException
     * @throws EmailHasInviteOnDirectoryException|UserNotConfirmedException
     */
    public function shareAction(Request $request, string $directoryUuid): JsonResponse
    {
        //Validation
        $this->validate($request, [
            'invitee_email' => 'required|email|max:255'
        ]);

        $shareService = new ShareService();
        $shareService->shareDirectory($this->user, $directoryUuid, $request->post('invitee_email'));
        return $this->respondSuccess();
    }

    /**
     * @param Request $request
     * @param string $directoryUuid
     * @return JsonResponse
     * @throws NotFoundModelException
     * @throws BaseException
     */
    public function leaveAction(Request $request, string $directoryUuid): JsonResponse
    {
        $shareService = new ShareService();
        $shareService->leaveDirectory($this->user, $directoryUuid);
        return $this->respondSuccess();
    }
}
