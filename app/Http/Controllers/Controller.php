<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    /**
     * @param array $data
     * @return JsonResponse
     */
    protected function respondSuccess(array $data = []):JsonResponse
    {
        return new JsonResponse([
            'success' => true,
            'data' => $data,
        ],200);
    }

    /**
     * @param string $message
     * @param int $errorCode
     * @return JsonResponse
     */
    protected function respondFailed(string $message = '', int $errorCode = -1):JsonResponse
    {
        return new JsonResponse([
            'success' => false,
            'message' => $message,
            'errorCode' => $errorCode,
        ], 400);
    }
}
