<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files_directories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('file_id');
            $table
                ->foreign('file_id')->references('id')->on('files')
                ->onDelete('CASCADE');
            $table->unsignedBigInteger('directory_id');
            $table
                ->foreign('directory_id')->references('id')->on('directories')
                ->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files_directories');
    }
}
