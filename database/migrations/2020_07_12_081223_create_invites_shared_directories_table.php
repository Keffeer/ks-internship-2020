<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitesSharedDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invites_shared_directories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('invite_id');
            $table
                ->foreign('invite_id')->references('id')->on('invites')
                ->onDelete('CASCADE');
            $table->unsignedBigInteger('directory_id');
            $table
                ->foreign('directory_id')->references('id')->on('directories')
                ->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invites_shared_directories');
    }
}
