<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_directories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table
                ->foreign('user_id')->references('id')->on('users')
                ->onDelete('CASCADE');
            $table->unsignedBigInteger('directory_id');
            $table
                ->foreign('directory_id')->references('id')->on('directories')
                ->onDelete('CASCADE');;
            $table->boolean('is_master');
            $table->unique(['user_id','directory_id'],'unique_one_user_one_role_on_directory');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_directories');
    }
}
